Optical Switch API
==================

This repo holds the optical switch API. The API itself is defined in [Protocol Buffers](https://developers.google.com/protocol-buffers) here

- https://gitlab.com/steamtb/osw-api/-/blob/master/osw.proto

Automatically generated documentation for the API is located here

- https://steamtb.gitlab.io/osw-api/

## Contributing

There are several ways to get involved

- Submit issues or start discussions [here](https://gitlab.com/steamtb/osw-api/-/issues)
- Fork this repo, update the API definition that covers the capabilities you need and submit a Merge request.
